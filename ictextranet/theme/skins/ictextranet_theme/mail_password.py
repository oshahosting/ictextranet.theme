## Script (Python) "mail_password"
##bind container=container
##bind context=context
##bind namespace=
##bind script=script
##bind subpath=traverse_subpath
##title=Mail a user's password
##parameters=

# Temporary fix. Might be removed when we use Zope 2.12.13
# Check https://syslab.com/proj/issues/1935 for more information

from Products.CMFPlone import PloneMessageFactory as pmf
REQUEST=context.REQUEST
try:
    response = context.portal_registration.mailPassword(REQUEST['userid'], REQUEST)
except:
    context.plone_utils.addPortalMessage("There was a problem sending you a password reminder. Did you use the right username?")
    response = context.mail_password_form()
return response
