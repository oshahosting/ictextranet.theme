from Acquisition import aq_inner, aq_base, aq_parent
from AccessControl import getSecurityManager
from DateTime import DateTime
from plone.app.content.browser.tableview import Table, TableKSSView
from plone.app.content.browser.foldercontents import FolderContentsTable
from plone.app.content.browser.foldercontents import FolderContentsView
from plone.indexer.delegate import DelegatingIndexerFactory
from slc.outdated.utils import is_outdated
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from Products.ATContentTypes.interface import IATTopic
from Products.CMFCore.utils import getToolByName
from Products.CMFCore.WorkflowCore import WorkflowException
from Products.CMFCore import permissions
from Products.CMFPlone import CatalogTool

import urllib
from zope.app.component.hooks import getSite
from zope.component import getMultiAdapter
from Products.CMFPlone.utils import safe_unicode
from Products.CMFPlone.utils import pretty_title_or_id, isExpired
from zope.i18n import translate


indexers = {}
for attr in dir(CatalogTool):
    mthd = getattr(CatalogTool, attr)
    # Reuse the indexer method where possible
    # The DelegatingIndexer for getIcon does not accept an argument
    # which causes a problem with plone.app.layout.icons.icons url
    if attr != "getIcon" and isinstance(mthd, DelegatingIndexerFactory):
        indexers[attr] = mthd


class Brain(object):
    """
    A proxy-brain object which returns properties directly from an
    object using the attributes expected from a brain.
    """
    def __init__(self, real):
        self.getId = real.getId()
        self.Description = real.Description()
        self.exclude_from_nav = real.exclude_from_nav()
        self.Type = real.Type()
        self.ModificationDate = real.ModificationDate()
        self.ExpirationDate = real.ExpirationDate()
        self.modified = real.modified()
        self.outdated = is_outdated(real)

        # The following need to be functions
        self.getObject = lambda : real
        self.getPath = lambda : "/".join(real.getPhysicalPath())
        self.getIcon = real.getIcon
        self.__providedBy__ = real.__providedBy__

        self.wf_tool = getToolByName(real, 'portal_workflow')

    def __getattr__(self, name):
        if name in indexers:
            attr = indexers[name](self.getObject())
            return attr()
        else:
            return getattr(aq_base(self.getObject()), name, '')

    __getitem__ = __getattr__

    def getObject(self, REQUEST=None):
        return self._realObject

    def getURL(self, relative = False):
        if relative:
            return self.getPath()
        else:
            return self.getObject().absolute_url()

    def pretty_title_or_id(self):
        for attr in 'Title', 'id':
            val = getattr(self, attr, None)
            if val:
                return val
        return '<untitled item>'

    @property
    def review_state(self):
        try:
            self.wf_tool.getInfoFor(self.getObject(), 'review_state')
        except WorkflowException:
            return ''

class ICTTable(Table):
    """ """
    render = ViewPageTemplateFile("templates/table.pt")

    def item_is_editable(self, item):
        portal = getSite()
        tool = getToolByName(portal, "portal_membership")
        obj = item["brain"].getObject()
        return bool(tool.checkPermission('Modify portal content', obj))

    def can_toggle_outdated(self, item):
        portal = getSite()
        tool = getToolByName(portal, "portal_membership")
        obj = item["brain"].getObject()
        return bool(tool.checkPermission('slc.ToggleOutdated', obj))


class ICTFolderContentsTable(FolderContentsTable):
    """ """

    def __init__(self, context, request, extended_rights, contentFilter={}):
        self.extended_rights = extended_rights
        super(ICTFolderContentsTable, self).__init__(context, request, contentFilter)
        url = context.absolute_url()
        view_url = url + '/@@folder_contents'
        self.table = ICTTable(
                            request, url, view_url, self.items,
                            show_sort_column=self.show_sort_column,
                            buttons=self.buttons, pagesize=10000)

    def getFilteredFolderContents(self,contentFilter=None,batch=False,b_size=100,full_objects=False):
        """ wrapper method around to use catalog to get folder contents
            extends getFolderContents and does not display folders which have excludeFromNav==False """
        context = self.context
        mtool = context.portal_membership
        cur_path = '/'.join(context.getPhysicalPath())
        path = {}

        if not contentFilter:
            contentFilter = {}
        else:
            contentFilter = dict(contentFilter)

        if not contentFilter.get('sort_on', None):
            contentFilter['sort_on'] = 'getObjPositionInParent'

        catalog_query_needed = True

        if contentFilter.get('path', None) is None:
            path['query'] = cur_path
            path['depth'] = 1
            contentFilter['path'] = path
            if contentFilter['sort_on']:
                catalog_query_needed = False

        show_inactive = mtool.checkPermission('Access inactive portal content', context)

        # Evaluate in catalog context because some containers override queryCatalog
        # with their own unrelated method (Topics)
        if catalog_query_needed:
            contents = context.portal_catalog.queryCatalog(contentFilter, show_all=1,
                                                              show_inactive=show_inactive)
        else:
            fullObjects = context.objectValues()
            fullObjects = [x for x in fullObjects if mtool.checkPermission('View', x)]
            contents = (Brain(x) for x in fullObjects if getattr(aq_base(x), 'portal_type', None))
            if not show_inactive:
                now = DateTime()
                contents = (x for x in contents if (not x.expires or not x.effective) or (x.expires() > now and x.effective() < now))

        # filter out folders appearing in the navigation
        filteredlist = []
        metaTypesNotToList = context.portal_properties.navtree_properties.getProperty('metaTypesNotToList')

        for b in contents:
            if 'outdated' in contentFilter and b.outdated != contentFilter['outdated']:
                continue
            if not hasattr(b, 'exclude_from_nav') or not hasattr(b, 'is_folderish') or self.extended_rights:
                # fallback for objects not having the attrs
                filteredlist.append(b)
                continue

            if b.exclude_from_nav == True or b.portal_type in metaTypesNotToList: # or b.review_state == 'private':
                filteredlist.append(b)

        contents = filteredlist

        if full_objects:
            contents = [b.getObject() for b in contents]

        if batch:
            from Products.CMFPlone import Batch
            b_start = context.REQUEST.get('b_start', 0)
            batch = Batch(contents, b_size, int(b_start), orphan=0)
            return batch

        return contents

    def contentsMethod(self):
        context = aq_inner(self.context)
        if IATTopic.providedBy(context):
            contentsMethod = context.queryCatalog
        else:
            contentsMethod = self.getFilteredFolderContents
        return contentsMethod

    @property
    def buttons(self):
        buttons = []
        context = aq_inner(self.context)
        portal_actions = getToolByName(context, 'portal_actions')
        button_actions = portal_actions.listActionInfos(object=aq_inner(self.context), categories=('folder_buttons', ))

        # Do not show buttons if there is no data, unless there is data to be
        # pasted
        if not len(self.items):
            show_buttons = ['import']
            if self.context.cb_dataValid():
                show_buttons.append('paste')
            return [self.setbuttonclass(x) for x in button_actions if x['id'] in show_buttons]

        for button in button_actions:
            # Make proper classes for our buttons
            if button['id'] != 'paste' or context.cb_dataValid():
                buttons.append(self.setbuttonclass(button))
        return buttons

    def folderitems(self):
        """
        """
        context = aq_inner(self.context)
        plone_utils = getToolByName(context, 'plone_utils')
        plone_view = getMultiAdapter((context, self.request), name=u'plone')
        plone_layout = getMultiAdapter((context, self.request), name=u'plone_layout')
        portal_workflow = getToolByName(context, 'portal_workflow')
        portal_properties = getToolByName(context, 'portal_properties')
        portal_types = getToolByName(context, 'portal_types')
        site_properties = portal_properties.site_properties

        use_view_action = site_properties.getProperty('typesUseViewActionInListings', ())
        browser_default = plone_utils.browserDefault(context)

        contentsMethod = self.contentsMethod()

        show_all = self.request.get('show_all', '').lower() == 'true'
        pagesize = 10000
        pagenumber = int(self.request.get('pagenumber', 1))
        start = (pagenumber - 1) * pagesize
        end = start + pagesize

        results = []
        for i, obj in enumerate(contentsMethod(self.contentFilter)):
            path = obj.getPath or "/".join(obj.getPhysicalPath())

            # avoid creating unnecessary info for items outside the current
            # batch;  only the path is needed for the "select all" case...
            # Include brain to make customizations easier (see comment below)
            if not show_all and not start <= i < end:
                results.append(dict(path=path, brain=obj))
                continue

            if (i + 1) % 2 == 0:
                table_row_class = "draggable even"
            else:
                table_row_class = "draggable odd"

            url = obj.getURL()
            icon = plone_layout.getIcon(obj)
            type_class = 'contenttype-' + plone_utils.normalizeString(
                obj.portal_type)

            review_state = obj.review_state
            state_class = 'state-' + plone_utils.normalizeString(review_state)
            if obj.outdated:
                state_class = ' '.join([state_class, 'slc-outdated'])
            relative_url = obj.getURL(relative=True)

            fti = portal_types.get(obj.portal_type)
            if fti is not None:
                type_title_msgid = fti.Title()
            else:
                type_title_msgid = obj.portal_type
            url_href_title = u'%s: %s' % (translate(type_title_msgid,
                                                    context=self.request),
                                          safe_unicode(obj.Description))

            modified = plone_view.toLocalizedTime(
                obj.ModificationDate, long_format=1)
            modified_sortable = 'sortabledata-' + obj.modified.strftime(
                '%Y-%m-%d-%H-%M-%S')

            if obj.portal_type in use_view_action:
                view_url = url + '/view'
            elif obj.is_folderish:
                view_url = url + "/folder_contents"
            else:
                view_url = url

            is_browser_default = len(browser_default[1]) == 1 and (
                obj.id == browser_default[1][0])

            size_sortable = 'sortabledata-{}'.format(obj.size()) if hasattr(obj.getObject(), 'size') else ''
            results.append(dict(
                # provide the brain itself to allow cleaner customisation of
                # the view.
                #
                # this doesn't add any memory overhead, a reference to
                # the brain is already kept through its getPath bound method.
                brain = obj,
                url = url,
                url_href_title = url_href_title,
                id = obj.getId,
                quoted_id = urllib.quote_plus(obj.getId),
                path = path,
                title_or_id = safe_unicode(pretty_title_or_id(plone_utils, obj)),
                obj_type = obj.Type,
                size = obj.getObjSize,
                size_sortable = size_sortable,
                modified = modified,
                modified_sortable = modified_sortable,
                icon = icon.html_tag(),
                type_class = type_class,
                wf_state = review_state,
                state_title = portal_workflow.getTitleForStateOnType(review_state,
                                                                 obj.portal_type),
                state_class = state_class,
                is_browser_default = is_browser_default,
                folderish = obj.is_folderish,
                relative_url = relative_url,
                view_url = view_url,
                table_row_class = table_row_class,
                is_expired = isExpired(obj),
                is_outdated = obj.outdated,
            ))
        return results


class ICTFolderContentsView(FolderContentsView):
    """ """

    def __init__(self, context, request):
        super(ICTFolderContentsView, self).__init__(context, request)
        self.contentFilter = {'outdated': False}
        if request.get('show_outdated', False):
            self.contentFilter = None

    @property
    def extended_rights(self):
        return getSecurityManager().checkPermission(permissions.ViewManagementScreens, self.context)

    def contents_table(self):
        table = ICTFolderContentsTable(
            aq_inner(self.context), self.request,
            self.extended_rights, contentFilter=self.contentFilter)
        return table.render()


class ICTFolderContentsKSSView(TableKSSView):
    table = ICTFolderContentsTable

    def __init__(self, context, request):
        super(ICTFolderContentsKSSView, self).__init__(context, request)

    @property
    def extended_rights(self):
        return getSecurityManager().checkPermission(permissions.ViewManagementScreens, self.context)

    def update_table(self, pagenumber='1', sort_on='getObjPositionInParent', show_all=False):
        self.request.set('sort_on', sort_on)
        self.request.set('pagenumber', pagenumber)
        table = self.table(self.context, self.request, self.extended_rights,
                                    contentFilter={'sort_on':sort_on})
        core = self.getCommandSet('core')
        core.replaceHTML('#folderlisting-main-table', table.render())
        return self.render()
