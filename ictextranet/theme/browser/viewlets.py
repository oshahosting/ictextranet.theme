from zope.interface import implements
from zope.viewlet.interfaces import IViewlet

from AccessControl import getSecurityManager
from Acquisition import aq_inner 
from Products.CMFCore.utils import getToolByName
from Products.CMFCore import permissions
from Products.PortalTransforms.transforms.safe_html import scrubHTML

from zope.component import getMultiAdapter

from plone.app.layout.viewlets.common import ViewletBase
from plone.app.layout.viewlets.common import SearchBoxViewlet
from plone.app.layout.viewlets.common import PersonalBarViewlet as PersonalBarViewletBase

from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile

class ViewMixin():
    """ This class provides helpful utility methods for view classes
    """

    def portal_url(self):
        """ """
        return getToolByName(self.context.aq_inner, 'portal_url')()


    def member(self):
        """ """
        pm = getToolByName(self.context.aq_inner, 'portal_membership')
        return pm.getAuthenticatedMember()


class ActionbarPanelViewlet(ViewletBase, ViewMixin):
    """ """
    implements(IViewlet)


class PollViewlet(ViewletBase, ViewMixin):
    """ """
    implements(IViewlet)

    def get_current_poll(self):
        """ Returns the first poll find in the current folder, or None if none
            is found.
        """
        security = getSecurityManager()
        polls = self.context.objectValues('PlonePopoll')
        for poll in polls:
            if security.checkPermission(permissions.View, poll):
                return poll

class CustomViewViewlet(ViewletBase):
    """
    Show the scrubbed contend of a File Object with customview.html Title
    """
    
    tmpl = '<div class="customview">%s</div>'

    def extractHTML(self, obj):
        portal_type = getattr(obj, 'portal_type', None)
        if portal_type == 'File':
            return obj.data
        elif portal_type == 'Document':
            return obj.getRawText()
        return ''

    def render(self):
        dirty_html = ''.join([self.extractHTML(x) 
                                for x in self.context.objectValues() 
                                if  x.title == 'customview.html'
                                and getattr(x, 'portal_type', None) in 
                                    ('File', 'Document')]
                                    [:1])
        if dirty_html:
            try:
                return self.tmpl % scrubHTML(dirty_html, raise_error=False)
            except AttributeError:
                return self.tmpl % dirty_html
        return ''

class ICTSearchBoxViewlet(SearchBoxViewlet):
    index = ViewPageTemplateFile('templates/searchbox.pt')

class PersonalBarViewlet(PersonalBarViewletBase):
    index = ViewPageTemplateFile('templates/personal_bar.pt')

    def update(self):
        super(PersonalBarViewlet, self).update()
        context = aq_inner(self.context)

        context_state = getMultiAdapter((context, self.request),
                                        name=u'plone_context_state')

        sm = getSecurityManager()
        self.user_actions = context_state.actions('user')
        self.anonymous = self.portal_state.anonymous()

        if not self.anonymous:
            member = self.portal_state.member()
            userid = member.getId()
            
            self.homelink_url = self.navigation_root_url + \
                '/personalize_form'

            membership = getToolByName(context, 'portal_membership')
            member_info = membership.getMemberInfo(member.getId())
            # member_info is None if there's no Plone user object, as when
            # using OpenID.
            if member_info:
                fullname = member_info.get('fullname', '')
            else:
                fullname = None
            if fullname:
                self.user_name = fullname
            else:
                self.user_name = userid
