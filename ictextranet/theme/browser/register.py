from AccessControl import getSecurityManager
from plone.app.users.browser.register import AddUserForm
from plone.i18n.normalizer.interfaces import IIDNormalizer
from Products.CMFCore.permissions import ManagePortal
from Products.CMFCore.utils import getToolByName
from Products.CMFPlone import PloneMessageFactory as _
from Products.statusmessages.interfaces import IStatusMessage
from zExceptions import Forbidden
from zope.component import getUtility
from zope.formlib import form


class ICTAddUserForm(AddUserForm):

    @form.action(_(u'label_register', default=u'Register'),
                 validator='validate_registration', name=u'register')
    def action_join(self, action, data):
        super(AddUserForm, self).handle_join_success(data)

        portal_groups = getToolByName(self.context, 'portal_groups')
        user_id = data['username']
        is_zope_manager = getSecurityManager().checkPermission(
            ManagePortal, self.context)

        try:
            # Add user to the selected group(s)
            if 'groups' in data.keys():
                for groupname in data['groups']:
                    group = portal_groups.getGroupById(groupname)
                    if 'Manager' in group.getRoles() and not is_zope_manager:
                        raise Forbidden
                    portal_groups.addPrincipalToGroup(user_id, groupname,
                                                      self.request)
        except (AttributeError, ValueError), err:
            IStatusMessage(self.request).addStatusMessage(err, type="error")
            return

        # HERE is the patch
        # After creation, rewrite the UID from email to a generated one
        # from the email
        search_by = user_id
        acl_users = getToolByName(self.context, 'acl_users')
        if getattr(acl_users, 'ldap', None):
            ldap_uf = acl_users.ldap.acl_users
            ldap_user = ldap_uf.getUserById(user_id)
            if ldap_user:
                dn = ldap_user.getUserDN()
                props = ldap_user._properties
                normalize = getUtility(IIDNormalizer).normalize
                uid = normalize(data['email'].replace(" ", "")).replace("-", "")
                props['uid'] = uid
                ldap_uf.manage_editUser(dn, props)
                search_by = data['email']
                # Fetch the property sheet, still under the old user_id
                mut = acl_users.mutable_properties
                prop_sheet = mut.getPropertiesForUser(ldap_user)
                # expire the user in LDAP UF
                ldap_uf._expireUser(ldap_user)
                # remove the old user_id from the mutable properties
                mut.deleteUser(user_id)
                # fetch the new ldap user
                new_ldap_user = ldap_uf.getUserById(uid)
                # Set the propertysheet under the new user
                mut.setPropertiesForUser(new_ldap_user, prop_sheet)
                # Now, in the PasswortResetTool, find the reset entry, and
                # replace the email with the uid, so that user will be able
                # to rest their PW from the initial email...
                pwr = getToolByName(self.context, 'portal_password_reset')
                for key, val in pwr._requests.items():
                    stored_user, timestamp = val
                    if stored_user == user_id:
                        pwr._requests[key] = (uid, timestamp)
                # And finally, handle group membership
                portal_groups = getToolByName(self.context, 'portal_groups')
                if 'groups' in data.keys():
                    for groupname in data['groups']:
                        group = portal_groups.getGroupById(groupname)
                        if 'Manager' in group.getRoles() and not is_zope_manager:
                            raise Forbidden
                        portal_groups.addPrincipalToGroup(uid, groupname,
                                                          self.request)

        IStatusMessage(self.request).addStatusMessage(
            _(u"User added."), type='info')
        self.request.response.redirect(
            self.context.absolute_url() +
            '/@@usergroup-userprefs?searchstring=' + search_by)
