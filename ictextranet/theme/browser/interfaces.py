from zope.interface import Interface
from plone.theme.interfaces import IDefaultPloneLayer

class IThemeSpecific(IDefaultPloneLayer):
    """Marker interface that defines a Zope 3 browser layer.
    """

class IICTExtranetLayer(Interface):
    """Marker Interface for a custom BrowserLayer
    """
