"""
https://syslab.com/proj/issues/show/1520

[R11] Share a folder - display a list of folders to which a user has
access

https://syslab.com/proj/issues/show/426

To get a list of all shared folders #1520 we will add a view which
returns links to the highest level folders the current user has access
to. First it queries the catalog for all Folders the user has access
to, then it filters out subfolders.

We will add a link to this view in the Actionbar:"My Workspaces".
"""

from Acquisition import aq_inner
from zope.app.component.hooks import getSite

from Products.Five import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile

class MemberDetailsView(BrowserView):
    """
    Display some details of the member
    """
    template = ViewPageTemplateFile("templates/member_details.pt")

    def __call__(self):
        context = self.context
        site = getSite()
        self.userid = context.getId()
        portal_membership = site.portal_membership

        portrait = portal_membership.getPersonalPortrait(self.userid);
        self.portrait_url = portrait.absolute_url()
        member = portal_membership.getMemberById(self.userid)

        self.fullname = member.getProperty("fullname")
        self.email = member.getProperty("email")
        self.home_page = member.getProperty("home_page")
        self.telephoneNumber = member.getProperty("telephoneNumber")
        self.facsimileTelephoneNumber = member.getProperty(
            "facsimileTelephoneNumber")
        self.ictHomeFacsimileTelephoneNumber = member.getProperty(
            "ictHomeFacsimileTelephoneNumber")
        self.homeTelephoneNumber = member.getProperty("homeTelephoneNumber")
        self.mobileTelephoneNumber = member.getProperty("mobileTelephoneNumber")
        self.homePostalAddress = member.getProperty("homePostalAddress")
        self.ictTimeZone = member.getProperty("ictTimeZone")
        self.alternativeEmail = member.getProperty("alternativeEmail")
        self.department = member.getProperty("department")
        self.biography = member.getProperty("description")
        self.location = member.getProperty("location")
        self.oshPersSubjects = member.getProperty("oshPersSubjects")
        self.ictFavoriteLinks = [i for i in
                                 member.getProperty("ictFavoriteLinks")
                                 if not i == "http://"]

        self.request.set("disable_border", True)
        return self.template()
