"""
https://syslab.com/proj/issues/show/1520

[R11] Share a folder - display a list of folders to which a user has
access

https://syslab.com/proj/issues/show/426

To get a list of all shared folders #1520 we will add a view which
returns links to the highest level folders the current user has access
to. First it queries the catalog for all Folders the user has access
to, then it filters out subfolders.

We will add a link to this view in the Actionbar:"My Workspaces".
"""

from Acquisition import aq_inner
from zope.app.component.hooks import getSite

from Products.Five import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile

class WorkspacesView(BrowserView):
    """
    Display the topmost folders which the current user has access to
    """
    template = ViewPageTemplateFile("templates/workspaces.pt")

    def __call__(self):
        self.request.set("disable_border", True)
        return self.template()

    def get_workspaces(self):
        """
        Return a list of brains of folders which the current user can view.

        This ignores top level folders which are configured in
        portal_properties/theme_properties, and doesn't return sub
        folders if the parent is also visible
        """
        site = getSite()
        portal_catalog = site.portal_catalog
        portal_properties = site.portal_properties

        # Filter some top level folders which everyone can view
        # e.g. the help center
        top_level_folders = set()
        for i in site.keys():
            if hasattr(site[i], "portal_type"):
                if getattr(site[i], "portal_type") == "Folder":
                    top_level_folders.add(i)
        ignore_folders = portal_properties.theme_properties.getProperty(
            "root_folders_which_are_not_workspaces")
        include_folders = top_level_folders.difference(ignore_folders)
        site_path = "/".join(site.getPhysicalPath())
        paths = [site_path+"/"+i for i in include_folders]

        query = {}
        query["portal_type"] = "Folder"
        query["path"] = paths

        folders = portal_catalog(query)

        # Get the highest visible folders in the tree:
        # 1. Order the paths by length
        # 2. Compare each path with the paths one level up
        # 3. If no path in the higher level is a parent we save this path
        # 4. Return the brains of the paths we have saved

        # {path:brain}
        path_brain = dict((i.physicalPath, i) for i in folders if i)

        # {depth:[path_a, path_b]}
        # Alternatively we could store physicalDepth in solr
        depth_paths = {}
        for path in path_brain.keys():
            depth = path.count("/")
            depth_paths.setdefault(depth, []).append(path)

        # Start with the deepest paths
        depths = sorted(depth_paths.keys())
        depths.reverse()
        workspace_paths = []
        for i, depth in enumerate(depths):
            # Ignore top-level folders since a shared workspace should
            # not be at this level. Top level paths have a depth of 2
            # because we're just counting the number of "/"s and the
            # path includes the path to the site root
            # e.g. /extranet/folder
            if depth > 2:
                for path in depth_paths[depth]:
                    parent_depth = depth - 1
                    if parent_depth not in depth_paths.keys():
                        workspace_paths.append(path)
                    else:
                        real_parent = "/".join(path.split("/")[:-1])
                        parent_paths = depth_paths[parent_depth]
                        if real_parent not in parent_paths:
                            workspace_paths.append(path)

        return [path_brain[i] for i in workspace_paths]
