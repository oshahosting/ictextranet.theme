from zope.component import adapts
from zope.formlib import form
from zope.interface import Interface
from zope.interface import implements
from zope import schema

from plone.app.form.widgets.uberselectionwidget import UberSelectionWidget
from plone.app.layout.navigation.root import getNavigationRoot
from plone.app.layout.navigation.interfaces import INavigationQueryBuilder
from plone.app.portlets.portlets import base
from plone.app.portlets.portlets.navigation import AddForm
from plone.app.portlets.portlets.navigation import EditForm
from plone.app.portlets.portlets.navigation import INavigationPortlet
from plone.app.portlets.portlets.navigation import Renderer
from plone.app.portlets.portlets.navigation import getRootPath

from plone.memoize.instance import memoize

from Products.CMFCore.utils import getToolByName
from Products.CMFPlone import PloneMessageFactory as _
from Products.CMFPlone import utils
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile

class IExtranetNavigationPortlet(INavigationPortlet):
    """ A custom navigation type portlet specifically for the XDV Extranet
        theme
    """

class NavigationRenderer(Renderer):
    _template = ViewPageTemplateFile('extranet_navigation.pt')
    recurse = ViewPageTemplateFile('navigation_recurse.pt')

    def __init__(self, context, request, view, manager, data):
        super(NavigationRenderer, self).__init__(context, request, view,
            manager, data)
        self.types = getToolByName(context, 'portal_types')

    def title(self):
        return 'Extranet Navigation'

    def getURL(self, node):
        default_view = self.types[node['portal_type']].default_view
        return '%s/%s' % (node['getURL'], default_view)


class Assignment(base.Assignment):
    implements(IExtranetNavigationPortlet)

    title = _(u'Extranet Navigation')
    name = u""
    root = None
    currentFolderOnly = True
    includeTop = False
    topLevel = 0
    bottomLevel = 0

class NavigationAddForm(base.NullAddForm):

    def create(self):
        return Assignment()


class QueryBuilder(object):
    """Build a navtree query based on the settings in navtree_properties
    and those set on the portlet.
    """
    implements(INavigationQueryBuilder)
    adapts(Interface, IExtranetNavigationPortlet)

    def __init__(self, context, portlet):
        self.context = context
        self.portlet = portlet
        portal_properties = getToolByName(context, 'portal_properties')
        navtree_properties = getattr(portal_properties, 'navtree_properties')
        portal_url = getToolByName(context, 'portal_url')

        # Acquire a custom nav query if available
        customQuery = getattr(context, 'getCustomNavQuery', None)
        if customQuery is not None and utils.safe_callable(customQuery):
            query = customQuery()
        else:
            query = {}

        currentPath = '/'.join(context.getPhysicalPath())
        query['path'] = {'query' : currentPath, 'depth': 2}
        query['portal_type'] = utils.typesToList(context)

        # Apply the desired sort
        sortAttribute = navtree_properties.getProperty('sortAttribute', None)
        if sortAttribute is not None:
            query['sort_on'] = sortAttribute
            sortOrder = navtree_properties.getProperty('sortOrder', None)
            if sortOrder is not None:
                query['sort_order'] = sortOrder

        # Filter on workflow states, if enabled
        if navtree_properties.getProperty('enable_wf_state_filtering', False):
            query['review_state'] = navtree_properties.getProperty('wf_states_to_show', ())

        show_outdated = context.REQUEST.get('show_outdated', False)
        if not show_outdated:
            query['isOutdated'] = False
        self.query = query

    def __call__(self):
        return self.query

