<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:dv="http://openplans.org/deliverance" xmlns:exsl="http://exslt.org/common" xmlns:xhtml="http://www.w3.org/1999/xhtml" version="1.0" exclude-result-prefixes="exsl dv xhtml">
  <xsl:output method="xml" indent="no" omit-xml-declaration="yes" media-type="text/html" encoding="utf-8" doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"/>

    <xsl:template match="/">

        <!-- Pass incoming content through initial-stage filter. -->
        <xsl:variable name="initial-stage-rtf">
            <xsl:apply-templates select="/" mode="initial-stage"/>
        </xsl:variable>
        <xsl:variable name="initial-stage" select="exsl:node-set($initial-stage-rtf)"/>

        <!-- Now apply the theme to the initial-stage content -->
        <xsl:variable name="themedcontent-rtf">
            <xsl:apply-templates select="$initial-stage" mode="apply-theme"/>
        </xsl:variable>
        <xsl:variable name="content" select="exsl:node-set($themedcontent-rtf)"/>

        <!-- We're done, so generate some output by passing 
            through a final stage. -->
        <xsl:apply-templates select="$content" mode="final-stage"/>

    </xsl:template>

    <!-- 
    
        Utility templates
    -->
    
    <xsl:template match="node()|@*" mode="initial-stage">
        <xsl:copy>
            <xsl:apply-templates select="node()|@*" mode="initial-stage"/>
        </xsl:copy>
    </xsl:template>
    <xsl:template match="/" mode="apply-theme">
        <html xmlns="http://www.w3.org/1999/xhtml"><head><base href="#"/><xsl:comment>[if lt IE 7]&gt;&lt;/base&gt;&lt;![endif]</xsl:comment><meta http-equiv="Content-Type" content="text/html; charset=utf-8"/><title>ICT Extranet</title></head><body>
    <div class="container_16">

	<img id="portal-logo" src="/extranet/logo.jpg"/><div class="thirddiv">
        <div id="search-container">
            <form id="livesearch0" action="http://osha.europa.eu/search" name="searchform" style="white-space: nowrap;">
                <label class="hiddenStructure" for="searchGadget"/>
                <div class="LSBox">
                    <input id="searchGadget" class="inputlabel inputlableActive" type="text" accesskey="4" title="Website durchsuchen" size="18" name="SearchableText" autocomplete="off"/><input type="submit" value="Suche" class="searchButton"/><div class="searchSection">
                        <input type="checkbox" value="/slc" name="path" class="noborder" id="searchbox_currentfolder_only"/><label style="cursor: pointer;" for="searchbox_currentfolder_only">nur im aktuellen Bereich</label>
                        </div> <xsl:comment> class="searchSection"</xsl:comment>
                    <div style="" id="LSResult" class="LSResult">
                        <div id="LSShadow" class="LSShadow"/>
                        </div> <xsl:comment> id="LSResult" class="LSResult"</xsl:comment>
                        <div class="searchSectionAdvanced">
                        <input type="checkbox" value="/slc" name="path" class="noborder" id="searchbox_currentfolder_only"/><label style="cursor: pointer;" for="searchbox_currentfolder_only">advanced search</label>
                    </div> <xsl:comment> class="searchSectionAdvanced"</xsl:comment>
                    <div style="" id="LSResult" class="LSResult"/>
                    <div id="LSShadow" class="LSShadow"/>
                </div> <xsl:comment> class="LSBox"</xsl:comment>
            </form>
        </div>
    </div>

	<xsl:comment>div class="clear"&gt;&lt;/div</xsl:comment>

	<div class="firstdiv">
	<h1 class="branding">
	  <a href="">EU-OSHA Extranet</a>
	<div id="main_link">
		<a href="http://osha.europa.eu">EU-Osha Main Site</a>
	</div>
	</h1>

	<div id="personaltools">
     <ul id="portal-personaltools">
         Login Logout Dashboard
     </ul></div>
	</div>
	<xsl:comment>div class="clear"&gt;&lt;/div</xsl:comment>



     <div class="clear"/>


      <div class="clear"/>

      <div class="grid_16" id="breadcrumbs">
        <span id="breadcrumbs-you-are-here">You are here:</span>
        <a href="http://osha.europa.eu/en">Home</a>
		<div class="clear"/>
	        <xsl:comment>&lt;div id="personaltools"&gt;
	            &lt;ul id='portal-personaltools'&gt;
	                Login Logout Dashboard
	            &lt;/ul&gt;
	        &lt;/div&gt;</xsl:comment>
      </div> <xsl:comment> class="grid_16"</xsl:comment>


        <div class="clear"><xsl:comment> </xsl:comment></div>

      <div class="grid_16" id="status-messages-container"/> <xsl:comment> class="grid_16"</xsl:comment>

<xsl:comment>      &lt;div class="grid_16" id="current-context-heading"&gt;
        &lt;h2 class="branding"&gt;
          EU-OSHA Extranet Library
        &lt;/h2&gt;
      &lt;/div&gt;
      </xsl:comment><xsl:comment> class="grid_16"</xsl:comment>
      <div class="clear"/>
      <div id="portlet-slot-1"/>



      <div id="navigation-divs">
        <div class="grid_4">
          <div class="box">
            <h2>Corporate Information</h2>
            <div class="block">
              <p>
                <a href="/company/press/press-releases" title="external-link">Work programmes</a>,
                <a href="/company/press/press-releases" title="external-link">Agency annual budgets</a>,
                <a href="/company/press/press-releases" title="external-link">Agency network structure</a>,
                <a href="/company/press/press-releases" title="external-link">Annual reports</a>,
                <a href="/company/press/press-releases" title="external-link">Evaluations</a>,
                <a href="/company/press/press-releases" title="external-link">H &amp;S Policy</a>,  ...
              </p>
            </div> <xsl:comment> class="block"</xsl:comment>
          </div> <xsl:comment> class="box"</xsl:comment>
        </div> <xsl:comment> class="grid_4"</xsl:comment>

        <div class="grid_4">
          <div class="box">
            <h2>Agency's planning (restricted access - FOPs, Bureau)</h2>
            <div class="block">
              <p>
                <a href="/company/press/press-releases" title="external-link">Background documents</a>,
                <a href="/company/press/press-releases" title="external-link">Earlier drafts</a>,
                <a href="/company/press/press-releases" title="external-link">Seminars</a>
              </p>
            </div> <xsl:comment> class="block"</xsl:comment>
          </div> <xsl:comment> class="box"</xsl:comment>
        </div> <xsl:comment> class="grid_4"</xsl:comment>

        <div class="grid_4">
          <div class="box">
            <h2>Board and Bureau</h2>
            <div class="block">
              <p>
                <a href="/company/press/press-releases" title="external-link">Advisory Groups</a>,
                <a href="/company/press/press-releases" title="external-link">Board</a>,
                <a href="/company/press/press-releases" title="external-link">Bureau </a>
              </p>
            </div> <xsl:comment> class="block"</xsl:comment>
          </div> <xsl:comment> class="box"</xsl:comment>
        </div> <xsl:comment> class="grid_4"</xsl:comment>

        <div class="grid_4">
          <div class="box">
            <h2>Focal point activities</h2>
            <div class="block">
              <p>
                <a href="/company/press/press-releases" title="external-link">Welcome pack</a>,
                <a href="/company/press/press-releases" title="external-link">Background documents </a>,
                <a href="/company/press/press-releases" title="external-link">EU Focal Points activities</a>,
                <a href="/company/press/press-releases" title="external-link">FoP Funding ECAP</a>,
                <a href="/company/press/press-releases" title="external-link">Meetings</a>,
                <a href="/company/press/press-releases" title="external-link">Members</a>,  ...
              </p>
            </div> <xsl:comment> class="block"</xsl:comment>
          </div> <xsl:comment> class="box"</xsl:comment>
        </div> <xsl:comment> class="grid_4"</xsl:comment>
        <div class="clear"/>

        <div class="grid_4">
          <div class="box">
            <h2>Expert groups</h2>
            <div class="block">
              <p>
                <a href="/company/press/press-releases" title="external-link">Agency Contact Group mainstreaming OSH in education &amp; training</a>,
                <a href="/company/press/press-releases" title="external-link"> Internet group</a>,
                <a href="/company/press/press-releases" title="external-link">Economic Incentives Group</a>,
                <a href="/company/press/press-releases" title="external-link">Former Groups</a>,
                <a href="/company/press/press-releases" title="external-link">HORECA group</a>,
                <a href="/company/press/press-releases" title="external-link">Risk Observatory group</a>
              </p>
            </div> <xsl:comment> class="block"</xsl:comment>
          </div> <xsl:comment> class="box"</xsl:comment>
        </div> <xsl:comment> class="grid_4"</xsl:comment>

        <div class="grid_4">
          <div class="box">
            <h2>New OSH Era</h2>
            <div class="block">
              <p>
                <a href="/company/press/press-releases" title="external-link">	Archive</a>,
                <a href="/company/press/press-releases" title="external-link">Management</a>,
                <a href="/company/press/press-releases" title="external-link">Work packages</a>
              </p>
            </div> <xsl:comment> class="block"</xsl:comment>
          </div> <xsl:comment> class="box"</xsl:comment>
        </div> <xsl:comment> class="grid_4"</xsl:comment>

        <div class="grid_4">
          <div class="box">
            <h2>Specific Networks</h2>
            <div class="block">
              <p>
                <a href="/company/press/press-releases" title="external-link">European Campaigns</a>,
                <a href="/company/press/press-releases" title="external-link">Archive</a>,
                <a href="/company/press/press-releases" title="external-link">Collaborating EU Agencies</a>,
                <a href="/company/press/press-releases" title="external-link">European Construction Safety Forum (ECSF)</a>,
                <a href="/company/press/press-releases" title="external-link">European network Education and Training in OSH</a>,
                <a href="/company/press/press-releases" title="external-link">National networks</a>,  ...
              </p>
            </div> <xsl:comment> class="block"</xsl:comment>
          </div> <xsl:comment> class="box"</xsl:comment>
        </div> <xsl:comment> class="grid_4"</xsl:comment>

        <div class="grid_4">
          <div class="box">
            <h2>Topic Centres</h2>
            <div class="block">
              <p>
                <a href="/company/press/press-releases" title="external-link">		TC-OSH</a>,
                <a href="/company/press/press-releases" title="external-link">Topic Centre Work Environment </a>
              </p>
            </div> <xsl:comment> class="block"</xsl:comment>
          </div> <xsl:comment> class="box"</xsl:comment>
        </div> <xsl:comment> class="grid_4"</xsl:comment>
    </div> <xsl:comment> id="navigation-divs" </xsl:comment>

    <div class="clear"/>

    <div id="portlet-slot-2"/>

    <div class="clear"/>

      <div class="grid_16" id="folder-contents">
        <table summary="Content listing" id="listing-table" class="listing"><thead><tr><th class="nosort">&#160;</th>
              <th id="foldercontents-title-column" class="nosort column">&#160;Title&#160;</th>
              <th id="foldercontents-size-column" class="nosort column">&#160;Size&#160;</th>
              <th id="foldercontents-modified-column" class="nosort column">&#160;Modified&#160;</th>
              <th id="foldercontents-status-column" class="nosort column">&#160;State&#160;</th>
              <th id="foldercontents-order-column" class="nosort column">
                &#160;Order&#160;</th>
            </tr></thead><tbody><tr id="folder-contents-item-index_html" class="draggable odd"><td class="notDraggable">
                <input type="checkbox" title="Select Press" alt="Select Press" value="/slc/company/press/index_html" id="cb_index_html" name="paths:list" class="noborder"/><input type="hidden" value="/company/press/index_html" name="selected_obj_paths:list"/><label for="cb_index_html" class="hiddenStructure">Press</label>
              </td>
              <td>
                <span class="contenttype-document">
                  <img width="16" height="16" alt="Page" src="document_icon.gif"/><a title="Page: " class="state-published" href="https://syslab.com/company/press/index_html">
                    <strong>Press</strong>
                  </a>
                </span>
              </td>
              <td> <span class="state-published">1.1 kB</span> </td>
              <td class="state-published">Jan 26, 2010 03:18 PM</td>
              <td> <span class="state-published">Published</span>
              </td> <td class="draggable draggingHook">::</td>
            </tr><tr id="folder-contents-item-press-releases-folder" class="draggable even"><td class="notDraggable">
                <input type="checkbox" title="Select Press releases" alt="Select Press releases" value="/slc/company/press/press-releases-folder" id="cb_press-releases-folder" name="paths:list" class="noborder"/><input type="hidden" value="/company/press/press-releases-folder" name="selected_obj_paths:list"/><label for="cb_press-releases-folder" class="hiddenStructure">Press releases</label>
              </td>
              <td>
                <span class="contenttype-folder">
                  <img width="16" height="16" alt="Folder" src="folder_icon.gif"/><a title="Folder: " class="state-published" href="https://syslab.com/company/press/press-releases-folder/folder_contents">
                    Press releases
                  </a>
                </span>
              </td>
              <td> <span class="state-published">1 kB</span> </td>
              <td class="state-published">Jan 26, 2010 03:14 PM</td>
              <td> <span class="state-published">Published</span> </td>
              <td class="draggable draggingHook">::</td>
            </tr><tr id="folder-contents-item-logo-syslab.eps" class="draggable odd"><td class="notDraggable">
                <input type="checkbox" title="Select logo_syslab.eps" alt="Select logo_syslab.eps" value="/slc/company/press/logo-syslab.eps" id="cb_logo-syslab.eps" name="paths:list" class="noborder"/><input type="hidden" value="/company/press/logo-syslab.eps" name="selected_obj_paths:list"/><label for="cb_logo-syslab.eps" class="hiddenStructure">logo_syslab.eps</label>
              </td>
              <td>
                <span class="contenttype-file">
                  <img width="16" height="16" alt="File" src="ps.png"/><a title="File: " class="state-published" href="https://syslab.com/company/press/logo-syslab.eps/view">
                    logo_syslab.eps
                  </a>
                </span>
              </td>
              <td> <span class="state-published">357.3 kB</span> </td>
              <td class="state-published">Jul 20, 2009 11:27 AM</td>
              <td> <span class="state-published">Published</span> </td>
              <td class="draggable draggingHook">::</td>
            </tr><tr id="folder-contents-item-logo-syslab.jpg" class="draggable even"><td class="notDraggable">
                <input type="checkbox" title="Select logo_syslab.jpg" alt="Select logo_syslab.jpg" value="/slc/company/press/logo-syslab.jpg" id="cb_logo-syslab.jpg" name="paths:list" class="noborder"/><input type="hidden" value="/company/press/logo-syslab.jpg" name="selected_obj_paths:list"/><label for="cb_logo-syslab.jpg" class="hiddenStructure">logo_syslab.jpg</label>
              </td>
              <td>
                <span class="contenttype-file">
                  <img width="16" height="16" alt="File" src="image_icon.gif"/><a title="File: " class="state-published" href="https://syslab.com/company/press/logo-syslab.jpg/view">
                    logo_syslab.jpg
                  </a>
                </span>
              </td>
              <td> <span class="state-published">466.6 kB</span> </td>
              <td class="state-published">Jul 20, 2009 11:27 AM</td>
              <td> <span class="state-published">Published</span> </td>
              <td class="draggable draggingHook">::</td>
            </tr><tr id="folder-contents-item-broschure05akt.pdf" class="draggable odd"><td class="notDraggable">
                <input type="checkbox" title="Select broschure05akt.pdf" alt="Select broschure05akt.pdf" value="/slc/company/press/broschure05akt.pdf" id="cb_broschure05akt.pdf" name="paths:list" class="noborder"/><input type="hidden" value="/company/press/broschure05akt.pdf" name="selected_obj_paths:list"/><label for="cb_broschure05akt.pdf" class="hiddenStructure">broschure05akt.pdf</label>
              </td>
              <td>
                <span class="contenttype-file">
                  <img width="16" height="16" alt="File" src="pdf_icon.gif"/><a title="File: " class="state-private" href="https://syslab.com/company/press/broschure05akt.pdf/view">
                    broschure05akt.pdf
                  </a>
                </span> <xsl:comment> class="contenttype-file" </xsl:comment>
              </td>
              <td> <span class="state-private">430.7 kB</span> </td>
              <td class="state-private">Nov 19, 2007 07:58 PM</td>
              <td> <span class="state-private">Private</span> </td>
              <td class="draggable draggingHook">::</td>
            </tr></tbody></table><xsl:comment> id="listing-table" class="listing" </xsl:comment></div> <xsl:comment> class="grid_16"</xsl:comment>
      <div class="clear"/>

      <div id="dockbottoms">
        <div id="dockbottoms-inner" class="dockbottoms-3 clearfix">
	  <div id="dockbottom-first" class="column">
	    <div id="block-author_pane-0" class="block block-author_pane region-odd odd region-count-1 count-9 block-inner content author-pane author-pane-inner">
	      <div class="author-pane-name-status author-pane-section">
	        <div class="author-pane-line author-name"><a title="File: " class="state-published" href="https://webmeeting.dimdim.com/portal/start.action?name=oshaextranet">
		    Start new meeting
		  </a>
	        </div>
	      </div>
	      <div class="author-pane-stats author-pane-section">
	      </div>
	      <div class="author-pane-admin author-pane-section">
	      </div>
	      <div class="author-pane-contact author-pane-section">
	      </div>
	    </div>
	  </div>
	  <div id="dockbottom-last" class="column">
	    <div id="block-block-1" class="block block-block region-odd odd region-count-1 count-11">
	      <div class="block-inner content" id="chat-wrapper">
	        <div id="chat">
                  <object type="application/x-shockwave-flash" data="http://widget.meebo.com/mm.swf?xsWyJwDhKU" width="160" height="250">
                    <param name="movie" value="http://widget.meebo.com/mm.swf?xsWyJwDhKU"/><param name="quality" value="high"/><param name="wmode" value="transparent"/></object>
	        </div>
	        <div class="slider"><a href="#" class="toggleup">Chat with Me</a>
	        </div>
	      </div>
	    </div>
	  </div>
        </div>
      </div>
    </div> <xsl:comment> class="container_16"</xsl:comment>

    <xsl:comment> temporary postit </xsl:comment>
    <xsl:comment>div id="support_div" style="display:none; z-index:1000; width:450px; height:410px; background:#eed427; position:absolute; left:150px; top:150px; margin:1em; padding:9em 6.5em; background:transparent url(postit.png) no-repeat scroll 0 0;"&gt;
        &lt;div style="-moz-transform:rotate(-3deg);-webkit-transform: rotate(-3deg); width: 400px"&gt;
        &lt;h1&gt;Welcome to the upgraded Extranet&lt;/h1&gt;
        &lt;p&gt;We have just upgraded the Extranet and you might need to &lt;a href="/mail_password_form"&gt;set a new password&lt;/a&gt;.&lt;/p&gt;
        &lt;p&gt;If you come across any problems or if you need any help, please don't hesitate to contact us at &lt;a href="mailto:oshanet@osha.europa.eu" title="external-link"&gt;oshanet@osha.europa.eu&lt;/a&gt;.&lt;/p&gt;

        &lt;p&gt;&lt;a class="closelink" onclick="jq('#support_div').hide();return false;" href="" style="text-decoration: underline;"&gt;close&lt;/a&gt;&lt;/p&gt;
        &lt;/div&gt;
        &lt;script language="JavaScript"&gt;
        var b = jq('form#login_form');
        if (b) { jq('#support_div').show();};
        &lt;/script&gt;
  &lt;/div</xsl:comment>


  </body></html>
    </xsl:template>
    <xsl:template match="style|script|xhtml:style|xhtml:script" priority="5" mode="final-stage">
        <xsl:element name="{local-name()}" namespace="http://www.w3.org/1999/xhtml">
            <xsl:apply-templates select="@*" mode="final-stage"/>
            <xsl:value-of select="text()" disable-output-escaping="yes"/>
        </xsl:element>
    </xsl:template>
    <xsl:template match="*" priority="3" mode="final-stage">
        <!-- Move elements without a namespace into 
        the xhtml namespace. -->
        <xsl:choose>
            <xsl:when test="namespace-uri(.)">
                <xsl:copy>
                    <xsl:apply-templates select="@*|node()" mode="final-stage"/>
                </xsl:copy>
            </xsl:when>
            <xsl:otherwise>
                <xsl:element name="{local-name()}" namespace="http://www.w3.org/1999/xhtml">
                    <xsl:apply-templates select="@*|node()" mode="final-stage"/>
                </xsl:element>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template match="node()|@*" priority="1" mode="final-stage">
        <xsl:copy>
            <xsl:apply-templates select="node()|@*" mode="final-stage"/>
        </xsl:copy>
    </xsl:template>

    <!-- 
    
        Extra templates
    -->
    
</xsl:stylesheet>